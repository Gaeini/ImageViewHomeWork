package ir.syborg.image;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;


public class AllScreen extends Activity {

    int       intInput;
    ImageView imgHundreds;
    ImageView imgDecimal;
    ImageView imgOnes;
    EditText  edtInput;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allscreen);

        imgHundreds = (ImageView) findViewById(R.id.imgHundreds);
        imgDecimal = (ImageView) findViewById(R.id.imgDecimal);
        imgOnes = (ImageView) findViewById(R.id.imgOnes);
        LinearLayout allScreen = (LinearLayout) findViewById(R.id.allScreen);
        allScreen.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                intInput++;

                if (intInput / 100 >= 1 && intInput / 100 < 10) {
                    switch (intInput / 100) {
                        case 1:
                            imgHundreds.setImageResource(R.drawable.one);
                            break;
                        case 2:
                            imgHundreds.setImageResource(R.drawable.two);
                            break;
                        case 3:
                            imgHundreds.setImageResource(R.drawable.three);
                            break;
                        case 4:
                            imgHundreds.setImageResource(R.drawable.four);
                            break;
                        case 5:
                            imgHundreds.setImageResource(R.drawable.five);
                            break;
                        case 6:
                            imgHundreds.setImageResource(R.drawable.six);
                            break;
                        case 7:
                            imgHundreds.setImageResource(R.drawable.seven);
                            break;
                        case 8:
                            imgHundreds.setImageResource(R.drawable.eight);
                            break;
                        case 9:
                            imgHundreds.setImageResource(R.drawable.nine);
                            break;
                    }

                }
                else {
                    imgHundreds.setImageResource(R.drawable.zero);
                }

                if ((intInput / 10) % 10 >= 1 && (intInput / 10) % 10 < 10) {
                    switch ((intInput / 10) % 10) {

                        case 1:
                            imgDecimal.setImageResource(R.drawable.one);
                            break;
                        case 2:
                            imgDecimal.setImageResource(R.drawable.two);
                            break;
                        case 3:
                            imgDecimal.setImageResource(R.drawable.three);
                            break;
                        case 4:
                            imgDecimal.setImageResource(R.drawable.four);
                            break;
                        case 5:
                            imgDecimal.setImageResource(R.drawable.five);
                            break;
                        case 6:
                            imgDecimal.setImageResource(R.drawable.six);
                            break;
                        case 7:
                            imgDecimal.setImageResource(R.drawable.seven);
                            break;
                        case 8:
                            imgDecimal.setImageResource(R.drawable.eight);
                            break;
                        case 9:
                            imgDecimal.setImageResource(R.drawable.nine);
                            break;
                    }
                }
                else {
                    imgDecimal.setImageResource(R.drawable.zero);
                }
                if (intInput % 10 >= 0 && intInput % 10 < 10) {
                    switch (intInput % 10) {
                        case 0:
                            imgOnes.setImageResource(R.drawable.zero);
                            break;
                        case 1:
                            imgOnes.setImageResource(R.drawable.one);
                            break;
                        case 2:
                            imgOnes.setImageResource(R.drawable.two);
                            break;
                        case 3:
                            imgOnes.setImageResource(R.drawable.three);
                            break;
                        case 4:
                            imgOnes.setImageResource(R.drawable.four);
                            break;
                        case 5:
                            imgOnes.setImageResource(R.drawable.five);
                            break;
                        case 6:
                            imgOnes.setImageResource(R.drawable.six);
                            break;
                        case 7:
                            imgOnes.setImageResource(R.drawable.seven);
                            break;
                        case 8:
                            imgOnes.setImageResource(R.drawable.eight);
                            break;
                        case 9:
                            imgOnes.setImageResource(R.drawable.nine);
                            break;
                    }

                }
            }
        });

    }
}
